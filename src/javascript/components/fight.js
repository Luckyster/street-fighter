import {controls} from '../../constants/controls';

export async function fight(first, second) {
	const firstHealth = document.querySelector('#left-fighter-indicator');
	const secondHealth = document.querySelector('#right-fighter-indicator');
	return new Promise((res) => {
		first.maxHealth = first.health;
		second.maxHealth = second.health;

		window.addEventListener('keydown', (e) => {
			if (e.code === controls.PlayerOneAttack) {
				second.health = second.health - getDamage(first, second);
				secondHealth.style.width = (100 / second.maxHealth) * second.health + '%';
				console.log(second);
				console.log(first);
				if(second.health <= 0) {
					res(first);
				}
			}
			if (e.code === controls.PlayerTwoAttack) {
				first.health = first.health - getDamage(second, first);
				firstHealth.style.width = (100 / first.maxHealth) * first.health + '%';
				console.log(second);
				console.log(first);
				if(first.health <= 0) {
					res(second);
				}
			}
			if (e.code === controls.PlayerOneBlock && first.block === false) first.block = true;
			if (e.code === controls.PlayerTwoBlock && second.block === false) second.block = true;


		})
		window.addEventListener('keyup', (e) => {
			if (e.code === controls.PlayerOneBlock) first.block = false;
			if (e.code === controls.PlayerTwoBlock) second.block = false;
		})
	});
}

export function getDamage(attacker, defender) {
	const  block = !defender.block ? defender.defense : getBlockPower(defender);
	const  attack = getHitPower(attacker);
	console.log(block, 'block');
	console.log(attack,'attacker');
	const damage = attack - block;
	return damage <= 0 ? 0 : damage;
}

export function getHitPower(fighter) {
	return fighter.attack * (Math.random() * 2 + 1);
}

export function getBlockPower(fighter) {
	return fighter.defense * (Math.random() * 2 + 1);
}
