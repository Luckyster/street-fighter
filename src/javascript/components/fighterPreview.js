import {createElement} from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
	const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
	const fighterElement = createElement({
		tagName: 'div',
		className: `fighter-preview___root ${positionClassName}`,
	});

	// todo: show fighter info (image, name, health, etc.)
	for (const [key, val] of Object.entries(fighter)) {
		let fighterInfo;
		switch (key) {
			case 'source':
				fighterInfo = createElement({
					tagName: 'img',
					className: 'preview-container___versus-img',
					attributes: {
						src: val,
					}
				})
				break;
			default:
				fighterInfo = createElement({
					tagName: 'div',
					className: 'preview-container___versus-block'
				});
				fighterInfo.appendChild(document.createTextNode(`${key}: ${val}`))	;
				break

		}
		fighterElement.appendChild(fighterInfo)

	}
	return fighterElement;
}

export function createFighterImage(fighter) {
	const {source, name} = fighter;
	const attributes = {
		src: source,
		title: name,
		alt: name
	};
	const imgElement = createElement({
		tagName: 'img',
		className: 'fighter-preview___img',
		attributes,
	});

	return imgElement;
}
